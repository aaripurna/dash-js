const path = require('path')
const webpack = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin');
const uglifyJsPlugin = require('terser-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractCSS = new ExtractTextPlugin('styles.min.css');
module.exports = {
	entry: './src/index.js',
	output: {
		library: 'dash',
		libraryTarget: 'umd',
        libraryExport: 'default',
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js'
	},
	module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.s?css/,
                use: extractCSS.extract([ 
                    'css-loader', 
                    'postcss-loader', 
                    'sass-loader'
                  ])
                
            }, 
            {
              test: /\.(svg|eot|ttf|woff|woff2)?$/,
              loader: "url-loader",
            }
        ]
    },
    plugins: [
        new uglifyJsPlugin(),
        new HTMLWebpackPlugin({
          filename: 'form.html',
          template: path.resolve(__dirname, 'form.html')
        }),
        new HTMLWebpackPlugin({
          template: path.resolve(__dirname, 'index.html')
        }),
        new HTMLWebpackPlugin({
          filename: 'auth.html',
          template: path.resolve(__dirname, 'auth.html')
        }),
        new webpack.HotModuleReplacementPlugin(),
        extractCSS
    ]
}