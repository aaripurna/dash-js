# DASH JS

## HOW TO INSTALL
```bash
  yarn add https://gitlab.com/aaripurna/dash-js.git
```

add this line to your javascript file and your're ready to go
```javascript
import * as Dash from 'dash-js/dist'
import "dash-js/dist/styles.min.css"

Dash.config({
  color: {
    'color-primary': '#caa629',
    'text-color-primary': '#5f5c68',
    'color-primary-light': '#d2b038',
    'text-color-grey': '#9a97a4',
    'dark-grey': '#5f5c68',
    'light-grey': '#f0f0f0',
    'text-blog-primary': '#545454',
    'text-blog-secondary': '#b3b3b3',
  },
  imageUploadUrl: '/dashboard/blob_uploads/',
  imageFetchUrl: '/dashboard/blob_uploads/blobs.json',
})

document.addEventListener('DOMContentLoaded', () => {
  Dash.start()
})
```

additionally you can define CSRF token 
```javascript
...

Dash.config({
	...

	csrfToken: '3W8Wq1Ktya2OJ2QTwMp/V4waPWOEcB2c0BT1yzxVDZgDzOMichNUxniLxjdZQOH6gjm310xTz2pvgMJ+qrvZ3A=='
})
...
```