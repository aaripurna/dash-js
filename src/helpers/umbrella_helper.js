import u from "umbrellajs"
import each from "lodash/each"

u.prototype.css = function(styles = {}) {
  const nodes = this
  nodes.each(function(node, _) {
    each(styles, (value, property) => {
      node.style[property] = value
    })
  })
  return nodes
};

u.prototype.hide = function() {
  this.each(function(node, _) {
    node.style.display = 'none'
  })
  return this
};

u.prototype.show = function() {
  this.each(function(node, _) {
    node.style.display = ''
  })
  return this
};

u.prototype.val = function(value = null) {
  if (value != undefined) {
    this.each((field, _) => {
      field.value = value
    })
    return this
  } else {
    return this.first().value
  }
};
u.prototype.exist = function() {
  return this.length >= 1
}