import SlimSelect from 'slim-select'
import u from 'umbrellajs'
import { get } from "axios"
import debounce from "lodash/debounce"
import _get from "lodash/get"

export default function() {
  u('.dash-selector-field').each((field, _) => {
    const type = u(field).data('type')
    const url = u(field).data('url')
    const options = JSON.parse(u(field).data('options')) || []
    const placehoder = u(field).data('placeholder') || "Type to search"

    const slimAjax = {
      ajax: function(term, callback) {
        debounce(
          get(`${url}?query=${term}`)
          .then(({ data }) => {
            if (_get(data, 'data')) {
              callback(data.data)
            } 
          })
          .catch(error => {
            callback(error)
          }), 300)
      }
    }

    const slimData = {
      data: options
    }

    let slimOptions = {
      select: u(field).find('.dash-selector-select').first(),
      searchPlaceholder: placehoder,
      searchText: "Yo Search somethin'"
    }

    if (url) {
      slimOptions = Object.assign(slimOptions, slimAjax)
    } else if (options.length > 0) {
      slimOptions = Object.assign(slimOptions, slimData)
    }

    if (type == 'search') {
      slimOptions  = Object.assign(slimOptions, {
        onChange: function(data) {
          if(_get({ data }, 'path')) {
            location.href = data.path
          }
        }
      }) 
    }

    const slimSelect = new SlimSelect(slimOptions)
  })
}