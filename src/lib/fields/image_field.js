import u from "umbrellajs"
import axios from 'axios'

export default function() {

  u('.image-field').each((field, _) => {

    const action = u(field).data('action')
    const url = u(field).data('url')
    const fieldName = u(field).data('name')
    u(field).find('.delete-image').on('click', (event) => {
      u(field).find('input').value = null
      if (action == 'edit') {
        axios.delete(`${url}/detach?attachment_name=${fieldName}`)
          .then((response) => {
            u(field).find('label').show()
            u(event.target).style.display.hide()
            u(field).find(".image-thumnail").hide()
          })
          .catch((error) => {
            alert(error.response)
          })
      } else {
        u(field).find("label").show()
        u(event.target).hide()
        u(field).find(".image-thumnail").hide()
      }

      u(field).find("input").on("change", (event) => {
        const self = event.target
        const file = self.files[0]
        const reader = new FileReader()
        const thumnail = u(field).find(".image-thumnail")

        reader.addEventListener("load", function () {
          thumnail.css({"background-image": `url(${reader.result})`})
          thumnail.show()
        }, false);

        if (file) {
          u(field).find(".delete-image").show()
          u(field).find("label").hide()
          reader.readAsDataURL(file)
        }
      })
    })
  })
}