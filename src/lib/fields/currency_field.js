import u from 'umbrellajs'

export default function() {
  u(".currency-field .shown-currency").each((field, _) => {
    u(field).on('keyup', (_) => {
      const realValue = field.value.replace(/\D|\.|^0/g,'')
      const decoratedVAlue = realValue.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      field.value = decoratedVAlue
      field.nextElementSibling.value = realValue
    })
    u(field).on('keydown', (event) => {
      const allowedKeys = ['Backspace', 'ArrowLeft', 'ArrowRight']
      const regex = /[\d]/
      if(!(regex.test(event.key) || allowedKeys.includes(event.key))) {
        event.preventDefault()
        return false
      }
    })
  })
}