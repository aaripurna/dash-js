import u from "umbrellajs"
import Dropzone from "dropzone"
import axios from "axios"

Dropzone.autoDiscover = false
export default function() {
  u('.galleries-field').each((field, _) => {
    const fileUploadUrl = u(`meta[name='image-upload-url']`).first().content
    const fileFetchUrl = u(`meta[name='file-fetch-url']`).first().content
    const csrfToken = u(`meta[csrf-token]`).first().content
    const dropzone = new Dropzone(`#${field.id}`, {
      removeLink: true,
      url: fileUploadUrl,
      headers: {
        'X-CSRF-TOKEN': csrfToken
      },
      removedFile: (file) => {
        axios.delete(fileUploadUrl + file.data.id)
          .then(response => {
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
          })
          .catch((error) => {
            alert(error.response)
          })
      },
      init: () => {
        const galleries = u(field).data('galleries')
        axios.get(fileFetchUrl, {params: {blob_ids: galleries}})
          .then(({ data }) => {
            each(data, (value, _) => {
              const mockFile = { name: value.filename, size: value.byte_size , data: value};
              dropzone.emit("addedfile", mockFile);
              dropzone.emit("thumbnail", mockFile, value.thumbnail);
              dropzone.emit("complete", mockFile);
            })
          })
          .catch(error => {
            alert(error.response)
          })
      }
    })
  })
}