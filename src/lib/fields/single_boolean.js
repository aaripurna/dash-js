import u from "umbrellajs"

export default function() {
  u('.single-boolean').each((field, _) => {
    const input = u(field).find("input[type='checkbox']")
    const hidden = u(field).find("input[type='hidden']")
    u(field).find('label').on('click', (_) => {
      hidden.val(!input.is(':checked'))
    })    
  })
}