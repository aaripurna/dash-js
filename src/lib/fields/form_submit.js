import u from "umbrellajs"
import axios from "axios"
import Swal from "sweetalert2"
import map from "lodash/map"


export default function() {
  u('.auto-submit').each((field, _) => {
    const primaryColor = getComputedStyle(document.documentElement).getPropertyValue('--color-primary')
    const form = u(field).closest('form')
    form.on('submit', (event) => {
      event.preventDefault()

      const url = form.attr('action')
      const method = u(form).find("[name='_method']").val() || u(form).attr("method")
      const data = new FormData(event.target)

      Swal.fire({
        title: "Are you sure?",
        type: "question",
        confirmButtonText: 'Submit',
        cancelButtonText: 'Cancel!',  
        confirmButtonColor: (primaryColor || '#caa629'),
        showCancelButton: true
      })
      .then(({ value }) => {
        if (value) {
          axios[method](`${url}.json`, data)
            .then(({ data }) => {
              if (data) {
                location.href = data.location
              }
            })
            .catch(({ response }) => {
              if (response && response.status == 422) {
                const totalError = response.data
                const messages = map(totalError, (value, key) => `${key.replace(/[^a-zA-Z]/g, " ")}: ${value.join(', ')}`)
                Swal.fire({
                  title: "Invalid Input!",
                  html: map(messages, (value) => `<p class="sweetalert-error-messages">${value}</p>`).join('\n'),
                  type: 'error',
                  confirmButtonColor: (primaryColor || '#caa629')
                })
              } else {
                Swal.fire({
                  title: "Something went wrong!",
                  type: 'error',
                  text: "If it happens continously. Please contact the developer!",
                  confirmButtonColor: (primaryColor || '#caa629')
                })
              }
            })
        }
      })

    })
  })
}