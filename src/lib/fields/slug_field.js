import u from "umbrellajs"

const slugify = (string) => {
  return string.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

export default function() {
  u('.slug-field input').each((field, _) => {

    const targetObject = u(field).data("object")
    const targetName = u(field).data("target")

    u(field).on('keydown', (event) => {
      const regex = /[\w-%\+=]/
      if (!regex.test(event.key)) {
        event.preventDefault()
        return false
      }
    })

    if (!targetName || !targetObject) return

    const target = `[name='${targetObject}[${targetName}]']`
    const targetDOM = u(target)
    targetDOM.on('keyup', (event) => {
      field.value = slugify(event.target.value)
    })
  })
}