import u from "umbrellajs"

export default function() {
  u('.text-array').each((field, _) => {

    const addButton = u(field).find('.add-button')
    const textInputField = u(field).find('.text-input')
    const fieldName = u(field).find('.text-input-box input').attr('name')

    u(field).find('i.remove-button').on('click', (event) => {
      u(event.target).parent('.text-input-box').remove()
    })

    addButton.on('click', (_) => {
      const input = u(`
        <div class="text-input-box">
          <input type="text" name="${fieldName}">
          <i class="remove-button fas fa-trash"></i>
        </div>
        `)
      textInputField.append(input)
      input.find('i.remove-button').on('click', (event) => {
        u(event.target).parent('.text-input-box').remove()
      })
    })

  })
}