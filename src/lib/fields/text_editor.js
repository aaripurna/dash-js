import Quill from "quill"
import u from "umbrellajs"
import ImageUpload from "quill-plugin-image-upload"

Quill.register('modules/imageUpload', ImageUpload)

export default function() {
  u('.text-editor').each((field, _) => {
    const input = field.querySelector('.text-editor-body')
    const hiddenField = u(field).find('textarea')
    const type = u(field).data('type') || 'basic'
    const placeholder = u(field).data('placeholder') 
    const defaultValue = hiddenField.val() || '<p></p>'

    const toolbarOptions = {
      blog: [
        ['bold', 'italic', 'underline', 'strike', 'link'],
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'align': [] }],
        ['image'],
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        ['clean']   
      ],
      basic: [
        ['bold', 'italic', 'underline', 'strike', 'link'],
        [{ 'color': [] }],  
        ['clean']   
      ]
    }

    const options = {
      modules: {
        toolbar: toolbarOptions[type],
        imageUpload: {
          upload: file => {
            return new Promise((resolve, reject) => {
              const form = new FormData();
              form.append("file", file, file.name)

              axios.post("/dashboard/blob_uploads/", form)
                .then((response) => {
                  const url = response.data.url
                  resolve(url)
                })
            })
          }
        }
      },
      placeholder: placeholder || "placeholder",
      theme: 'snow'
    };

   const editor = new Quill(input, options)
    editor.root.innerHTML = defaultValue

    editor.on("text-change", (e) => {
      const value = editor.root.innerHTML
      hiddenField.value = value
    })
  })
}