import currencyField from "./currency_field"  
import imageField from "./image_field"  
import textArray from "./text_array"  
import slugField from "./slug_field"  
import singleBoolean from "./single_boolean"
import textEditor from "./text_editor"
import slimSelect from "./slim_select"
import galleries from "./galleries_field"
import formSubmit from "./form_submit"

module.exports =  {
  currencyField,
  imageField,
  textArray,
  slugField,
  singleBoolean,
  textEditor,
  slimSelect,
  galleries,
  formSubmit
}