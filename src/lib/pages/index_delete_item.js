import u from "umbrellajs"
import axios from "axios"
import Swal from "sweetalert2"

export default function() {
  u('.dashboard-delete-item').each((button, _) => {
    const primaryColor = getComputedStyle(document.documentElement).getPropertyValue('--color-primary')
    u(button).on('click', (event) => {
      event.preventDefault()
      Swal.fire({
        title: 'Are You Sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: (primaryColor || '#caa629'),
        cancelButtonColor: '#5f5c68',
      }).then((result) => {
        if (result.value) {
          axios.delete(`${u(button).data("url")}.json`)
            .then((response) => {
              Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                '#caa629'
              )
              u(button).closest(".item-row").remove()
            })
            .catch((err) => {
              Swal.fire({
                title: "Something went wrong!",
                type: 'error',
                text: "If it happens continously. Please contact the developer!",
                confirmButtonColor: (primaryColor || '#caa629')
              })
            }) 
        }
      })
    })
  })
}