import u from "umbrellajs"

export default function() {
  let ddButton = ""
  u('a.dropdown-toggle').on('click', function(event){
    ddButton = this
    u(this).siblings('div').css({display: 'inline-block'})
  })
  u(document).on('click', function(event){
    if(u(event.target) != ddButton && u(event.target).closest('a.dropdown-toggle').first() != ddButton ) {
      u('.dropdown-menu').css({display: 'none'})
    }
  })
}