import indexDeleteItem from "./index_delete_item"
import dropdown from "./dropdown"

module.exports = {
  indexDeleteItem,
  dropdown
}