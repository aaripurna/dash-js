import axios from 'axios'
import fields  from '../fields'
import pages from "../pages"
import each from "lodash/each"
import u from "umbrellajs"

const start = () => {
  each(fields, (method, _) => {
      method()
    })
    each(pages, (method, _) => {
      method()
    })
}

const setCSRFToken = (params = {}) => {
  const meta = u("meta[name='csrf-token']")
  let token = ""
  if (meta.exist() && params['csrfToken'] && meta.first().content == params['csrfToken']) {
    token = params['csrfToken']
  } else if (meta.exist() && params['csrfToken'] && meta.first().content != params['csrfToken']) {
    meta.first().content = params['csrfToken']
    token = params['csrfToken']
  } else if (meta.exist() && !params['csrfToken']) {
    token = params['csrfToken']
  } else {
    u('head').append(u(`<meta name='csrf-token' content='${params['csrfToken']}'/>`))
    token = params['csrfToken']
  }
  axios.defaults.headers.common['X-CSRF-TOKEN'] = token
}

const setColorVariable = (params = {}) => {
  const rootStyle = document.documentElement.style
  if(params['color']) {
    each(params.color, (value, key) => {
      rootStyle.setProperty(`--${key}`, value)
    })
  }
}

const setImageUploadUrl = (options = {}) => {
  u('head').append(u(`<meta name='image-upload-url' content='${options['imageUploadUrl']}'>`))
  u('head').append(u(`<meta name='file-fetch-url' content='${options['imageFetchUrl']}'>`))
}


const config = (options = {}) => {
  setCSRFToken(options)
  setColorVariable(options)
  setImageUploadUrl(options)
}

export { start,  config}