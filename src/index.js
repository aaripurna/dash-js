import "./scss/main.scss"	
import "@fortawesome/fontawesome-free/css/fontawesome.min.css"
import "@fortawesome/fontawesome-free/css/solid.min.css"
import "./helpers"
import * as Dash from "./lib/dash"
import u from "umbrellajs"

// Dash.config({
//   color: {
//     'color-primary': '#caa629',
//     'text-color-primary': '#5f5c68',
//     'color-primary-light': '#d2b038',
//     'text-color-grey': '#9a97a4',
//     'dark-grey': '#5f5c68',
//     'light-grey': '#f0f0f0',
//     'text-blog-primary': '#545454',
//     'text-blog-secondary': '#b3b3b3',
//   },
//   imageUploadUrl: '/dashboard/blob_uploads/',
//   imageFetchUrl: '/dashboard/blob_uploads/blobs.json',
//   csrfToken: '3W8Wq1Ktya2OJ2QTwMp/V4waPWOEcB2c0BT1yzxVDZgDzOMichNUxniLxjdZQOH6gjm310xTz2pvgMJ+qrvZ3A=='
// })
// // Dash.start()

// document.addEventListener('DOMContentLoaded', (_) => {
//   // debugger
// Dash.start()
// })
export default Dash